<?php

namespace Plugins\Address\Traits;

use Plugins\Address\Entities\Address;

trait HasAddress
{
    /**
     * Address entitites
     *
     */
    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    /**
     * Address entitites
     *
     */
    public function address()
    {
        return $this->morphOne(Address::class, 'addressable');
    }
}
