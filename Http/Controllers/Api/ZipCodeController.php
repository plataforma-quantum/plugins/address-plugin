<?php

namespace Plugins\Address\Http\Controllers\Api;

use Illuminate\Routing\Controller;

class ZipCodeController extends Controller
{

    /**
     * Lista all cities
     *
     */
    public function show($zip)
    {
        // Load states
        $address = _q('address')->service('zipCode')->findAddress($zip);
        return response()->json([
            'status'  => $address ? 200 : 400,
            'success' => $address ? 'ok' : 'not',
            'data'    => $address
        ], $address ? 200 : 400);
    }
}
