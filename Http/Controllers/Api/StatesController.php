<?php

namespace Plugins\Address\Http\Controllers\Api;

use Illuminate\Routing\Controller;

class StatesController extends Controller
{

    /**
     * Lista all states
     *
     */
    public function index()
    {
        // Load states
        $states = _q('address')->service('states')->findAll();

        // Check if it is complete
        $states->map(function ($state) {
            if (request()->query('complete', false)) {
                $state->citites;
            }
            return $state;
        });

        return response()->json([
            'status'  => 200,
            'success' => 'ok',
            'data'    => $states
        ], 200);
    }

    /**
     * Get state by id
     *
     */
    public function show($state)
    {
        // Load states
        $state = _q('address')->service('states')->find($state);

        // Check if it is complete
        if (request()->query('complete', false)) {
            $state->citites;
        }

        // Show response
        return response()->json([
            'status'  => $state ? 200 : 400,
            'success' => $state ? 'ok' : 'not',
            'data'    => $state
        ], $state ? 200 : 400);
    }
}
