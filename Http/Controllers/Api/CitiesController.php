<?php

namespace Plugins\Address\Http\Controllers\Api;

use Illuminate\Routing\Controller;

class CitiesController extends Controller
{

    /**
     * Lista all cities
     *
     */
    public function index()
    {
        // Load states
        $cities = _q('address')->service('cities')->with('state')->get();

        return response()->json([
            'status'  => 200,
            'success' => 'ok',
            'data'    => $cities
        ], 200);
    }

    /**
     * Get state by id
     *
     */
    public function show($city)
    {
        // Load states
        $city = _q('address')->service('cities')->with('state')->find($city);

        // Show response
        return response()->json([
            'status'  => $city ? 200 : 400,
            'success' => $city ? 'ok' : 'not',
            'data'    => $city
        ], $city ? 200 : 400);
    }
}
