<?php

namespace Plugins\Address\Http\Controllers\Admin;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class StatesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Estados';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(_q('address')->service('states')->getModel());

        $grid->disableExport();

        $grid->column('id', __('Código'))->sortable();
        $grid->column('letter', __('UF'));
        $grid->column('title', __('Estado'));
        $grid->column('iso', __('IBGE'));
        $grid->column('population', __('População'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(_q('address')->service('states')->findOrFail($id));

        $show->field('id', __('Código'));
        $show->field('letter', __('UF'));
        $show->field('title', __('Estado'));
        $show->field('slug', __('Slug'));
        $show->field('iso', __('IBGE'));
        $show->field('population', __('População'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(_q('address')->service('states')->getModel());

        $form->text('title', 'Estado')->required();
        $form->text('letter', 'UF')->required();
        $form->text('slug', 'Slug')->required();
        $form->text('iso', 'IBGE')->required();
        $form->text('population', 'População')->required();

        return $form;
    }
}
