<?php

namespace Plugins\Address\Http\Controllers\Admin;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class CitiesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Cidades';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(_q('address')->service('cities')->getModel());

        $grid->disableExport();

        $grid->column('id', __('Código'))->sortable();
        $grid->column('title', __('Cidade'));
        $grid->column('state.letter', __('UF'));
        $grid->column('state.title', __('Estado'));
        $grid->column('iso_ddd', __('DDD'));
        $grid->column('iso', __('IBGE'));
        $grid->column('population', __('População'));

        $grid->filter(function ($filter) {
            $states = _q('address')->service('states')->findAll();
            $filter->in('state_id', __('Estado'))->select($states->pluck('title', 'id'));
            $filter->like('title', __('Cidade'));
            $filter->like('iso_ddd', __('DDD'));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(_q('address')->service('cities')->findOrFail($id));

        $show->field('id', __('Código'));
        $show->field('state.title', __('Estado'));
        $show->field('state.letter', __('UF'));
        $show->field('title', __('Cidade'));
        $show->field('slug', 'Slug');
        $show->field('iso', 'IBGE');
        $show->field('iso_ddd', 'DDD');
        $show->field('population', 'DDD');
        $show->field('lat', 'Latitude');
        $show->field('long', 'Longitude');
        $show->field('income_per_capita', 'Renda per capita');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $states = _q('address')->service('states')->findAll();

        $form = new Form(_q('address')->service('cities')->getModel());

        $form->select('state_id', 'Estado')->options($states->pluck('title', 'id'))->required();
        $form->text('title', 'Cidade')->required();
        $form->text('slug', 'Slug')->required();
        $form->text('iso', 'IBGE')->required();
        $form->text('iso_ddd', 'DDD')->required();
        $form->text('population', 'DDD')->required();
        $form->text('lat', 'Latitude')->required();
        $form->text('long', 'Longitude')->required();
        $form->text('income_per_capita', 'Renda per capita')->required();

        return $form;
    }
}
