<?php

namespace Plugins\Address\Services;

use Illuminate\Support\Facades\Http;

class ZipCodeService
{

    /**
     * Gets and address by cep
     *
     */
    public function findAddress(string $cep)
    {

        // Make the request to via cep
        $response = Http::get("https://viacep.com.br/ws/$cep/json/");

        // Check if it is success
        if (!$response->ok()) return null;
        if ($response->status() !== 200) return null;

        // Get json
        $data = $response->json();

        // Find city
        $city = _q('address')->service('cities')->findOneBy('iso', 'like', $data['ibge']);

        // Return formatted address
        return [
            'zipcode'      => $data['cep'],
            'address'      => $data['logradouro'],
            'complement'   => $data['complemento'],
            'neighborhood' => $data['bairro'],
            'city'         => $city,
            'state'        => $city->state()->get()
        ];
    }
}
