<?php

namespace Plugins\Address\Services;

use Plugins\Address\Entities\State;
use Quantum\Models\Service;

class StateService extends Service
{

    /**
     * Service model
     *
     */
    protected $model = State::class;
}
