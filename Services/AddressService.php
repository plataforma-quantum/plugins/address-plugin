<?php

namespace Plugins\Address\Services;

use Plugins\Address\Entities\Address;
use Quantum\Models\Service;

class AddressService extends Service
{

    /**
     * Service model
     *
     */
    protected $model = Address::class;
}
