<?php

namespace Plugins\Address\Services;

use Plugins\Address\Entities\City;
use Quantum\Models\Service;

class CityService extends Service
{

    /**
     * Service model
     *
     */
    protected $model = City::class;
}
