<?php

namespace Plugins\Address\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AddressDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(SeedAdminMenuTableSeeder::class);
        $this->call(SeedStatesTableSeeder::class);
        $this->call(SeedCitiesTableSeeder::class);
    }
}
