<?php

namespace Plugins\Address\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SeedAdminMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_menu')->where('plugin', 'address')->delete();
        $parentId = DB::table('admin_menu')->insertGetId([
            'parent_id' => 0,
            'order' => 80,
            'title' => 'Endereços',
            'plugin' => 'address',
            'icon' => 'fa-map',
            'uri' => null,
            'permission' => NULL,
            'created_at' => NULL,
            'updated_at' => '2020-06-01 17:54:28'
        ]);
        DB::table('admin_menu')->insert([[
            'parent_id' => $parentId,
            'order' => 1,
            'title' => 'Estados',
            'plugin' => 'address',
            'icon' => 'fa-map-marker',
            'uri' => 'address/states',
            'permission' => NULL,
            'created_at' => NULL,
            'updated_at' => '2020-06-01 17:54:28',
        ], [
            'parent_id' => $parentId,
            'order' => 2,
            'title' => 'Cidades',
            'plugin' => 'address',
            'icon' => 'fa-map-marker',
            'uri' => 'address/cities',
            'permission' => NULL,
            'created_at' => NULL,
            'updated_at' => '2020-06-01 17:54:28',
        ]]);
    }
}
