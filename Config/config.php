<?php

return [

    /**
     * Plugin name
     *
     */
    'name' => 'Address',

    /**
     * Plugin services
     *
     */
    'services' => [
        'states'  => Plugins\Address\Services\StateService::class,
        'cities'  => Plugins\Address\Services\CityService::class,
        'zipCode' => Plugins\Address\Services\ZipCodeService::class,
        'address' => Plugins\Address\Services\AddressService::class,
    ]
];
