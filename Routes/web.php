<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

$config = [
    'prefix'     => 'admin/address',
    'middleware' => ['admin']
];

Route::group($config, function (Router $router) {
    $router->resource('states', 'Admin\\StatesController');
    $router->resource('cities', 'Admin\\CitiesController');
});
