<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$config = [
    'middleware' => 'api',
    'prefix'     => 'address'
];

Route::group($config, function (Router $router) {
    $router->resource('states', 'Api\\StatesController')->only(['index', 'show']);
    $router->resource('cities', 'Api\\CitiesController')->only(['index', 'show']);
    $router->resource('zipcode', 'Api\\ZipCodeController')->only(['show']);
});
