<?php

namespace Plugins\Address\Entities;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

    /**
     * Model table name
     *
     */
    protected $table = 'address_addresses';

    /**
     * Guarded properties
     *
     */
    protected $guarded = [];

    /**
     * BelongsTo City Relationship
     *
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * HasOneThrough State Relationship
     *
     */
    public function state()
    {
        $this->hasOneThrough(State::class, City::class);
    }
}
