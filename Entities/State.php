<?php

namespace Plugins\Address\Entities;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{

    /**
     * Table name on database
     *
     */
    protected $table = 'address_states';

    /**
     * Guarded properties
     *
     */
    protected $guarded = [];

    /**
     * HasMany Cities Relationship
     *
     */
    public function citites()
    {
        return $this->hasMany(City::class);
    }
}
