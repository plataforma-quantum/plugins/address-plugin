<?php

namespace Plugins\Address\Entities;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    /**
     * Model name on database
     *
     */
    protected $table = "address_cities";

    /**
     * Guarded fields
     *
     */
    protected $guarded = [];

    /**
     * BelongsTo State Relationship
     *
     */
    public function state()
    {
        return $this->belongsTo(State::class);
    }
}
